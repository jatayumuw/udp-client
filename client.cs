using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.IO;



namespace Udp_Client
{
    class program
    {
        static void Main(string[] args)
        {
            string message  = "";

            UdpClient udpc = new UdpClient("10.252.39.113", 12000);
            IPEndPoint ipep = null;

            Console.Write("Input ID: ");
            string ClientID = Console.ReadLine();
            byte[] ncdata = Encoding.ASCII.GetBytes(ClientID);
            udpc.Send(ncdata, ncdata.Length, ipep);

            byte[] nsdata = udpc.Receive(ref ipep);
            string ServerID = Encoding.ASCII.GetString(nsdata);
            Console.WriteLine("\n" + ServerID + " Connected \n");

            while (message != "bye")
            {
                Console.Write("message: ");
                message = Console.ReadLine();
                byte[] sdata = Encoding.ASCII.GetBytes(message);
                udpc.Send(sdata, sdata.Length);

                byte[] rdata = udpc.Receive(ref ipep);
                string job = Encoding.ASCII.GetString(rdata);
                Console.WriteLine("Server : " + job);
            }
        }
    }
}
